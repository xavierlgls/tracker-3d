<?php require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'header.php'; ?>
<style>
    .fas.fa-filter {
        cursor: pointer;
    }

    .fas.fa-filter:hover {
        color: orange;
    }
</style>
<h1>Vendée Globe 2020 rankings</h1>
<p>Ranks computed for each mark in the course. This is an estimation based on a database of locations saved every 30min.</p>
<table class="table table-hover">
    <thead>
        <tr>
            <th>Contestant</th>
            <th><i class="fas fa-filter" filter="filter-sailaway"></i> Rank</br><small>automatically computed</small></th>
            <th><i class="fas fa-filter" filter="filter-equator-down"></i> Equator</br><small>latitude: 0°</small></th>
            <th><i class="fas fa-filter" filter="filter-good-hope"></i> Good hope</br><small>longitude: 18.473°</small></th>
            <th><i class="fas fa-filter" filter="filter-leeuwin"></i> Leeuwin</br><small>longitude: 115.1313°</small></th>
            <th><i class="fas fa-filter" filter="filter-antimeridial"></i> Antimeridial</br><small>longitude: 180°</small></th>
            <th><i class="fas fa-filter" filter="filter-horn"></i> Horn</br><small>longitude: -67.289°</small></th>
            <th><i class="fas fa-filter" filter="filter-equator-up"></i> Equator back</br><small>latitude: 0°</small></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
<script src="script.js"></script>
<?php require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'footer.php'; ?>