const startDate = new Date("2020-11-08 12:02:00");
let boats = {};

$(document).ready(function () {
    $.get('https://cveserver.online/public/backend/rest/get-tracker-vg.php?action=get-rankings', function (result) {
        result.boats.forEach(boat => {
            boats[boat.id] = {
                skipper: boat.owner,
                name: boat.name,
                id: boat.id,
                rank: parseInt(boat.computed_rank),
                distance: boat.distance_to_leader == null ? null : parseInt(boat.distance_to_leader),
                date_equatorDown: boat.date_equatorDown == null ? null : new Date(boat.date_equatorDown),
                date_capeLeeuwin: boat.date_capeLeeuwin == null ? null : new Date(boat.date_capeLeeuwin),
                date_antimeridial: boat.date_antimeridial == null ? null : new Date(boat.date_antimeridial),
                date_capeOfGoodHope: boat.date_capeOfGoodHope == null ? null : new Date(boat.date_capeOfGoodHope),
                date_equatorUp: boat.date_equatorUp == null ? null : new Date(boat.date_equatorUp),
                date_capeHorn: boat.date_capeHorn == null ? null : new Date(boat.date_capeHorn)
            };
        });

        // Equator down

        let rank = 1;
        Object.values(boats).filter(x => x.date_equatorDown != null).sort((a, b) => {
            return a.date_equatorDown - b.date_equatorDown;
        }).forEach(item => {
            boats[item.id].rank_equatorDown = rank++;
        });

        // Cape of good hope

        rank = 1;
        Object.values(boats).filter(x => x.date_capeOfGoodHope != null).sort((a, b) => {
            return a.date_capeOfGoodHope - b.date_capeOfGoodHope;
        }).forEach(item => {
            boats[item.id].rank_capeOfGoodHope = rank++;
        });

        // Cape Leeuwin

        rank = 1;
        Object.values(boats).filter(x => x.date_capeLeeuwin != null).sort((a, b) => {
            return a.date_capeLeeuwin - b.date_capeLeeuwin;
        }).forEach(item => {
            boats[item.id].rank_capeLeeuwin = rank++;
        });

        // Antimeridial

        rank = 1;
        Object.values(boats).filter(x => x.date_antimeridial != null).sort((a, b) => {
            return a.date_antimeridial - b.date_antimeridial;
        }).forEach(item => {
            boats[item.id].rank_antimeridial = rank++;
        });

        // Cape Horn

        rank = 1;
        Object.values(boats).filter(x => x.date_capeHorn != null).sort((a, b) => {
            return a.date_capeHorn - b.date_capeHorn;
        }).forEach(item => {
            boats[item.id].rank_capeHorn = rank++;
        });

        // Equator up

        rank = 1;
        Object.values(boats).filter(x => x.date_equatorUp != null).sort((a, b) => {
            return a.date_equatorUp - b.date_equatorUp;
        }).forEach(item => {
            boats[item.id].rank_equatorUp = rank++;
        });



        // Display
        $('.fas.fa-filter').each(function () {
            $(this).click(function () {
                sortTable($(this).attr('filter'));
            });
        });
        sortTable('');

    })
});

function displayBoat(boat) {
    let row = '<tr>';
    row += `<td>${boat.skipper}</br><small class="text-secondary">${boat.name}</small></td>`;
    row += `<td>${boat.rank == 0 ? 'DNF' : (boat.rank + (boat.distance != null ? `</br><small class="text-secondary">distance to leader: ${boat.distance} nm</small>`: ''))}</td>`;
    row += `<td>${boat.rank_equatorDown ? `${boat.rank_equatorDown}</br><small class="text-secondary">${timeSinceDepartureString(boat.date_equatorDown)}</small>` : '-'}</td>`;
    row += `<td>${boat.rank_capeOfGoodHope ? `${boat.rank_capeOfGoodHope}</br><small class="text-secondary">${timeSinceDepartureString(boat.date_capeOfGoodHope)}</small>` : '-'}</td>`;
    row += `<td>${boat.rank_capeLeeuwin ? `${boat.rank_capeLeeuwin}</br><small class="text-secondary">${timeSinceDepartureString(boat.date_capeLeeuwin)}</small>` : '-'}</td>`;
    row += `<td>${boat.rank_antimeridial ? `${boat.rank_antimeridial}</br><small class="text-secondary">${timeSinceDepartureString(boat.date_antimeridial)}</small>` : '-'}</td>`;
    row += `<td>${boat.rank_capeHorn ? `${boat.rank_capeHorn}</br><small class="text-secondary">${timeSinceDepartureString(boat.date_capeHorn)}</small>` : '-'}</td>`;
    row += `<td>${boat.rank_equatorUp ? `${boat.rank_equatorUp}</br><small class="text-secondary">${timeSinceDepartureString(boat.date_equatorUp)}</small>` : '-'}</td>`;
    row += '</tr>';
    $('tbody').append(row);
}

function timeSinceDepartureString(date) {
    let rest = date - startDate;
    const days = Math.floor(rest / (24 * 3600 * 1000));
    rest -= (days * 24 * 3600 * 1000);
    const hours = Math.floor(rest / (3600 * 1000));
    rest -= (hours * 3600 * 1000);
    const minutes = Math.floor(rest / (60 * 1000));
    return `${days}d ${hours}h ${minutes}m`;
}

function sortTable(type) {
    let sortFunction;
    switch (type) {
        case 'filter-equator-down':
            sortFunction = (a, b) => {
                if (!a.rank_equatorDown) {
                    return 1;
                } else if (!b.rank_equatorDown) {
                    return -1;
                } else {
                    return a.rank_equatorDown - b.rank_equatorDown;
                }
            };
            break;
        case 'filter-good-hope':
            sortFunction = (a, b) => {
                if (!a.rank_capeOfGoodHope) {
                    return 1;
                } else if (!b.rank_capeOfGoodHope) {
                    return -1;
                } else {
                    return a.rank_capeOfGoodHope - b.rank_capeOfGoodHope;
                }
            };
            break;
        case 'filter-leeuwin':
            sortFunction = (a, b) => {
                if (!a.rank_capeLeeuwin) {
                    return 1;
                } else if (!b.rank_capeLeeuwin) {
                    return -1;
                } else {
                    return a.rank_capeLeeuwin - b.rank_capeLeeuwin;
                }
            };
            break;
        case 'filter-antimeridial':
            sortFunction = (a, b) => {
                if (!a.rank_antimeridial) {
                    return 1;
                } else if (!b.rank_antimeridial) {
                    return -1;
                } else {
                    return a.rank_antimeridial - b.rank_antimeridial;
                }
            };
            break;
        case 'filter-horn':
            sortFunction = (a, b) => {
                if (!a.rank_capeHorn) {
                    return 1;
                } else if (!b.rank_capeHorn) {
                    return -1;
                } else {
                    return a.rank_capeHorn - b.rank_capeHorn;
                }
            };
            break;
        case 'filter-equator-up':
            sortFunction = (a, b) => {
                if (!a.rank_equatorUp) {
                    return 1;
                } else if (!b.rank_equatorUp) {
                    return -1;
                } else {
                    return a.rank_equatorUp - b.rank_equatorUp;
                }
            };
            break;
        default:
            sortFunction = (a, b) => {
                if (a.rank == 0) {
                    return 1;
                } else if (b.rank == 0) {
                    return -1;
                } else {
                    return a.rank - b.rank;
                }
            };
    }
    $('tbody').empty();
    Object.values(boats).sort(sortFunction).forEach(item => {
        displayBoat(item);
    });
}