# backend

* `cron-tracker-vg.php`: executé périodiquement par une tâche cron pour récupérer les données de course dans Sailaway
* `get-tracker-vg.php`: utilisé pour récupérer les données stockées sur le serveur

# frontend

* `index.html`: page d'accueil pour l'interface de visualisation du tracker