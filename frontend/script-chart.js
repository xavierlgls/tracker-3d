let chart1, chart2, chart3;

function initChart() {
    if ($('input[name="username"]').length) {
        const username = decodeURI($('input[name="username"]').val());
        const boatsFiltered = boats.filter(x => {
            return decodeURI(x.skipper) == username;
        });
        if (boatsFiltered.length > 0) {
            const boat = boatsFiltered[0];
            $.get(`../../backend/rest/get-tracker-vg.php?action=get-race-stats&id=${boat.id}`, onRaceStats);
        }
    } else {
        const boat = boats.reduce((a, b) => {
            if (a.rank > b.rank) {
                return b;
            } else {
                return a;
            }
        });
        $.get(`../../backend/rest/get-tracker-vg.php?action=get-race-stats&id=${boat.id}`, onRaceStats);
    }

}

function onRaceStats(result) {
    if (result.success) {
        $('#user-name-stats').text(boats[parseInt(result.id)].skipper)
        displayStats(result);
    }
}

function displayStats(result) {
    if (chart1) {
        chart1.clear();
        chart1.destroy();
    }
    if (chart2) {
        chart2.clear();
        chart2.destroy();
    }
    if (chart3) {
        chart3.clear();
        chart3.destroy();
    }
    const stats = result.stats;
    const speedData = stats.map(sample => {
        return {
            t: new Date(Date.parse(sample.date)),
            y: parseFloat(sample.speed)
        }
    });
    const windSpeedData = stats.map(sample => {
        return {
            t: new Date(Date.parse(sample.date)),
            y: parseFloat(sample.wind_speed)
        }
    });
    const rankData = stats.map(sample => {
        return {
            t: new Date(Date.parse(sample.date)),
            y: parseInt(sample.rank)
        }
    });
    const windAngleData = stats.map(sample => {
        const windDir = parseInt(sample.wind_direction);
        const heading = parseInt(sample.heading);
        let windAngle = Math.abs(windDir - heading);
        if (windAngle < 0) {
            windAngle += 180;
        } else if (windAngle > 180) {
            windAngle -= 180;
        }
        return {
            t: new Date(Date.parse(sample.date)),
            y: windAngle
        }
    });


    chart1 = new Chart(document.getElementById('time1').getContext('2d'), {
        type: 'line',
        data: {
            datasets: [{
                label: 'Boat speed (knt)',
                data: speedData,
                borderColor: "#3e95cd",
                fill: false
            }, {
                label: 'Wind speed (knt)',
                data: windSpeedData,
                borderColor: "#fc5214",
                fill: false
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        unit: 'day'
                    }
                }]
            },
            elements: {
                point: {
                    radius: 0
                }
            }
        }
    });


    chart2 = new Chart(document.getElementById('time2').getContext('2d'), {
        type: 'line',
        data: {
            datasets: [{
                label: 'Rank',
                data: rankData,
                borderColor: "#34cc1d",
                fill: false
            }, {
                label: 'Wind angle (°)',
                data: windAngleData,
                borderColor: "#fcb103",
                fill: false
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        unit: 'day'
                    }
                }]
            },
            elements: {
                point: {
                    radius: 0
                }
            }
        }
    });


    const step = 5;
    let angles = [];
    let hist = [];
    for (let i = -step / 2; i <= 360 - step / 2; i += step) {
        angles.push(i + step / 2);
        const count = windAngleData.filter(x => {
            return x.y >= i && x.y < i + step;
        }).length / windAngleData.length * 100;
        hist.push(count);
    }

    chart3 = new Chart(document.getElementById('polar').getContext('2d'), {
        type: 'radar',
        data: {
            labels: angles,
            datasets: [{
                label: 'wind angles repartition over time (%)',
                data: hist,
                backgroundColor: 'rgba(34, 138, 212, 0.5)',
                borderColor: '#228ad4'
            }]
        },
        options: {
            elements: {
                point: {
                    radius: 0
                }
            }
        }
    });

}