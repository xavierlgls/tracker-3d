<?php
require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'header.php';
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vendée Globe 2020 - Sailaway</title>
    <link rel="icon" href="../../assets/vg_favicon.png?" type="image/x-icon">
    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- cesium -->
    <script src="https://cesium.com/downloads/cesiumjs/releases/1.123/Build/Cesium/Cesium.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://cesium.com/downloads/cesiumjs/releases/1.123/Build/Cesium/Widgets/widgets.css" rel="stylesheet">
    <style>
        .main {
            position: relative;
        }

        #cesiumContainer {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }

        #dashboard {
            font-family: 'Poppins', sans-serif;
            display: block;
            position: absolute;
            top: 5px;
            left: 5px;
            color: white;
            opacity: 0.7;
            background-color: rgb(21, 46, 69);
            border: 1px solid rgb(226, 6, 19);
            border-radius: 8px;
            min-width: 150px;
            max-width: 25vw;
        }

        #dash-title {
            text-align: center;
            margin: 0.2em;
        }

        #dash-header {
            display: flex;
            justify-content: space-between;
            align-items: flex-end;
            font-size: 0.6em;
            font-weight: 500;
            padding-left: 1em;
            padding-right: 22px;
            color: rgb(226, 6, 19);
        }

        #dash-footer {
            text-align: center;
            font-size: 0.6em;
        }

        #sailors-container {
            overflow-y: scroll;
            max-height: 40vh;
            padding: 4px;
        }

        .sailor-container {
            display: flex;
            justify-content: space-between;
            font-size: 0.8em;
            margin: 0.1em;
            border-top: solid 1px #8296A5;
        }

        .clickable {
            cursor: pointer;
        }

        span.clickable:hover {
            color: black;
            background-color: blanchedalmond;
        }

        span.clickable:active {
            color: black;
            background-color: rgb(226, 6, 19);
        }

        img.clickable:hover {
            filter: grayscale(100%) sepia(1) saturate(1) brightness(90%) hue-rotate(358deg);
            -webkit-filter: grayscale(100%) sepia(1) saturate(1) brightness(90%) hue-rotate(358deg);
        }

        img.clickable:active {
            filter: brightness(0.5) sepia(1) saturate(10000%);
        }

        .icons img {
            margin-right: 5px;
        }

        .rank {
            color: #e2001a;
            font-weight: 800;
        }

        a {
            color: #8296A5;
        }

        .favorite {
            background-color: rgba(226, 6, 19, 0.5);
        }

        input[name="sailor-filter"] {
            height: 1em;
            width: auto;
        }
    </style>
</head>

<body>
    <h1>Tracker <a href="../vg2020-rankings" class="btn btn-warning">New: rankings !</a></h1>
    <div class="main">
        <div id="cesiumContainer" class="fullSize"></div>
        <div id="dashboard">
            <h2 id="dash-title" class="clickable" onclick="dashboardToggle()">Skippers</h2>
            <div>
                <img class="" src="assets/search.png" height="15">
                <input type="text" name="sailor-filter">
            </div>
            <div id="dash-header" style="display: flex;"><span>sailaway rank</span>
                <img id="fleet-toggle" src="assets/fleet_selection.png" class="clickable" height="20" title="display all the fleet" onclick="fleetModeToggle()">
            </div>
            <div id="sailors-container" style="display: block;"></div>
            <div id="dash-footer">powered by <a href="http://cveserver.online">complete voyage experience</a></div>
        </div>
    </div>
    <script src="boat.js"></script>
    <h1>Boat stats <span id="user-name-stats"></span></h1>
    <div class="container">
        <div>
            <select name="player-selector" class="form-control"></select>
            <?php if (isset($_SESSION['auth'])) : ?>
                <input type="hidden" name="username" value="<?= $_SESSION['auth']->getUsername() ?>">
            <?php endif ?>
        </div>
        <div>
            <canvas id="time1" width="auto" height="100"></canvas>
            <canvas id="time2" width="auto" height="100"></canvas>
            <canvas id="polar" width="auto" height="auto"></canvas>
        </div>
    </div>
    <!-- chart -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="script-chart.js"></script>
    <script src="script.js"></script>
</body>

</html>

<?php require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'footer.php'; ?>