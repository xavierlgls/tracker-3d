class Boat {
    constructor(data) {
        this._data = data;
        this._visible = false;
        this._fleetMode = false;

        // setup all Cesium components
        this._boatPositionProperty = new Cesium.SampledPositionProperty();
        this._labelPositionProperty = new Cesium.SampledPositionProperty();
        this._orientationsProperty = new Cesium.SampledProperty(Cesium.Quaternion);
        this._labelTextsProperty = new Cesium.TimeIntervalCollectionProperty();
        this._modelEntity = null;
        this._lineEntity = null;
        this._labelEntity = null;
        this._positions = null;

        // dashboard
        let str = `<div class="sailor-container ${this.favorite ? "favorite" : ""}" id="sailor-${this.id}">`;
        str += `<span>`;
        str += `<span class="rank">${this.rank}</span>`;
        str += ` <span class="clickable skipper" title="click to fly to this boat" onclick="goToSailor(${this.id})">${this.skipper}</span> `;
        str += `</span>`;
        str += `<span class="icons">`;
        str += `<img class="clickable favorite-icon" title="favorite saved" src="${this.favorite ? toggleFavoriteOnSrc : toggleFavoriteOffSrc}" height="15" onclick="onToggleFavorite(${this.id})">`;
        str += `<img class="clickable display-icon" title="toggle display" src="assets/eye-slashed.png" height="15"  onclick="onToggleDisplay(${this.id})">`;
        str += `</span>`;
        str += `</div>`;
        $('#sailors-container').append(str);
    }

    onPositions(positions) {
        this._positions = positions;

        let previousTime = null;
        positions.forEach(pos => {
            const time = Cesium.JulianDate.fromDate(new Date(Date.parse(pos.date)));

            const boatPosition = Cesium.Cartesian3.fromDegrees(parseFloat(pos.lng), parseFloat(pos.lat), 0);
            this._boatPositionProperty.addSample(time, boatPosition);

            const labelPosition = Cesium.Cartesian3.fromDegrees(parseFloat(pos.lng), parseFloat(pos.lat), 6e3);
            this._labelPositionProperty.addSample(time, labelPosition);

            var pitch = parseInt(pos.starboard) ? 12 : -12;
            var head = Cesium.Math.toRadians(parseInt(pos.heading));
            var hpr = new Cesium.HeadingPitchRoll(head, Cesium.Math.toRadians(pitch), Cesium.Math.toRadians(0));
            const orientation = Cesium.Transforms.headingPitchRollQuaternion(
                boatPosition,
                hpr
            );
            this._orientationsProperty.addSample(time, orientation);

            if (previousTime) {
                this._labelTextsProperty.intervals.addInterval(new Cesium.TimeInterval({
                    start: time,
                    stop: previousTime,
                    isStartIncluded: false,
                    isStopIncluded: true,
                    data: `${this.skipper} (${pos.rank})`
                }));

            }

            previousTime = time;
        });

        if (this._fleetMode) {
            if (this.favorite) {
                this.displayAsFavorite();
            } else {
                this.displayAsStandard();
            }
        } else if (this.visible) {
            if (this.favorite) {
                this.displayAsFavorite();
            } else {
                this.displayAsStandard();
            }
        }
    }

    displayAsFavorite() {
        let positions = this._positions;
        let boatPositionsPropery = this._boatPositionProperty;

        if (!this._fleetMode || true) {
            this._lineEntity = viewer.entities.add({
                name: `trajectory ${this.id}`,
                polyline: {
                    positions: new Cesium.CallbackProperty(function (time, result) {
                        if (time.equals(lastUpdateTime)) {
                            //TODO: return the last computed positions
                        }
                        lastUpdateTime = time;
                        let currentBoatPos = null;
                        let degreesArray = [];
                        let end = false;
                        positions.sort((a, b) => (new Date(Date.parse(a.date))) - (new Date(Date.parse(b.date))));
                        positions.forEach(position => {
                            if (!end) {
                                const currentTime = Cesium.JulianDate.fromDate(new Date(Date.parse(position.date)));
                                if (Cesium.JulianDate.greaterThan(time, currentTime)) {
                                    if (Cesium.JulianDate.secondsDifference(time, currentTime) < Boat.favoritePathDuration * 3600) {
                                        degreesArray.push(parseFloat(position.lng));
                                        degreesArray.push(parseFloat(position.lat));
                                    }
                                } else {
                                    currentBoatPos = boatPositionsPropery.getValue(time);
                                    end = true;
                                }
                            }
                        });
                        let output = Cesium.Cartesian3.fromDegreesArray(
                            degreesArray,
                            Cesium.Ellipsoid.WGS84
                        );
                        if (currentBoatPos) {
                            output.push(currentBoatPos);
                        }
                        return output;
                    }, false),
                    width: 4,
                    clampToGround: true,
                },
            });
            makeFavoriteProperty(this._lineEntity, Cesium.Color.RED.withAlpha(0.2));
        }

        var modelUri = "3d_models/" + this._data.texture_name + ".glb";
        if (this.rank == 1) {
            modelUri = "3d_models/leader.glb";
        }
        this._modelEntity = viewer.entities.add({
            name: `boat ${this.id}`,
            availability: new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({
                start: startDate,
                stop: stopDate
            })]),
            position: this._boatPositionProperty,
            orientation: this._orientationsProperty,
            model: {
                uri: modelUri,
                scale: 10.0 * (this._fleetMode ? 2 : 1),
                shadows: Cesium.ShadowMode.DISABLED
            },
        });

        this._labelEntity = viewer.entities.add({
            name: `label ${this.id}`,
            availability: new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({
                start: startDate,
                stop: stopDate
            })]),
            position: this._labelPositionProperty,
            label: {
                text: this._labelTextsProperty,
                scaleByDistance: new Cesium.NearFarScalar(1.5e2, 1.5, 8.0e5, 0.3),
                horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
                verticalOrigin: Cesium.VerticalOrigin.TOP,
                disableDepthTestDistance: Number.POSITIVE_INFINITY,
                fillColor: Cesium.Color.RED,
                distanceDisplayCondition: new Cesium.DistanceDisplayCondition(0, 4e6)
            },
        });
    }

    displayAsStandard() {
        let positions = this._positions;
        let boatPositionsPropery = this._boatPositionProperty;

        if (!this._fleetMode) {
            this._lineEntity = viewer.entities.add({
                name: `trajectory ${this.id}`,
                polyline: {
                    positions: new Cesium.CallbackProperty(function (time, result) {
                        if (time.equals(lastUpdateTime)) {
                            //TODO: return the last computed positions
                        }
                        lastUpdateTime = time;
                        let currentBoatPos = null;
                        let degreesArray = [];
                        let end = false;
                        positions.sort((a, b) => (new Date(Date.parse(a.date))) - (new Date(Date.parse(b.date))));
                        positions.forEach(position => {
                            if (!end) {
                                const currentTime = Cesium.JulianDate.fromDate(new Date(Date.parse(position.date)));
                                if (Cesium.JulianDate.greaterThan(time, currentTime)) {
                                    if (Cesium.JulianDate.secondsDifference(time, currentTime) < Boat.standardPathDuration * 3600) {
                                        degreesArray.push(parseFloat(position.lng));
                                        degreesArray.push(parseFloat(position.lat));
                                    }
                                } else {
                                    currentBoatPos = boatPositionsPropery.getValue(time);
                                    end = true;
                                }
                            }
                        });
                        let output = Cesium.Cartesian3.fromDegreesArray(
                            degreesArray,
                            Cesium.Ellipsoid.WGS84
                        );
                        if (currentBoatPos) {
                            output.push(currentBoatPos);
                        }
                        return output;
                    }, false),
                    width: 4,
                    clampToGround: true,
                },
            });
            makeStandardProperty(this._lineEntity, Cesium.Color.AQUA.withAlpha(0.2));
        }

        var modelUri = "3d_models/" + this._data.texture_name + ".glb";
        if (this.rank == 1) {
            modelUri = "3d_models/leader.glb";
        }
        this._modelEntity = viewer.entities.add({
            name: `boat ${this.id}`,
            availability: new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({
                start: startDate,
                stop: stopDate
            })]),
            position: this._boatPositionProperty,
            orientation: this._orientationsProperty,
            model: {
                uri: modelUri,
                scale: 10.0 * (this._fleetMode ? 10 : 1),
                shadows: Cesium.ShadowMode.DISABLED
            },
        });

        this._labelEntity = viewer.entities.add({
            name: `label ${this.id}`,
            availability: new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({
                start: startDate,
                stop: stopDate
            })]),
            position: this._labelPositionProperty,
            label: {
                text: this._labelTextsProperty,
                scaleByDistance: new Cesium.NearFarScalar(1.5e2, 1.5, 8.0e5, 0.3),
                horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
                verticalOrigin: Cesium.VerticalOrigin.TOP,
                disableDepthTestDistance: Number.POSITIVE_INFINITY,
                fillColor: Cesium.Color.WHITE.withAlpha(0.5),
                distanceDisplayCondition: new Cesium.DistanceDisplayCondition(0, 4e6)
            },
        });
    }

    hide() {
        viewer.entities.remove(this._lineEntity);
        viewer.entities.remove(this._modelEntity);
        viewer.entities.remove(this._labelEntity);
    }

    flyTo() {
        if (this._positions) {
            const heading = 0;
            const pitch = -Math.PI / 8;
            const range = 6e4;
            viewer.flyTo(this._modelEntity, {
                offset: new Cesium.HeadingPitchRange(heading, pitch, range)
            });
        }
    }

    set fleetMode(value) {
        this._fleetMode = value;
        this.hide();
        if (value) {
            if (this._positions) {
                if (this.favorite) {
                    this.displayAsFavorite();
                } else {
                    this.displayAsStandard();
                }
            } else {
                $.get(`../../backend/rest/get-tracker-vg.php?action=get-boat-trajectory&id=${this.id}`, onPositionsReceived);
            }
        } else if (this.visible) {
            if (this.favorite) {
                this.displayAsFavorite();
            } else {
                this.displayAsStandard();
            }
        }
    }

    get id() {
        return parseInt(this._data.id);
    }

    get rank() {
        return parseInt(this._data.rank);
    }

    get skipper() {
        return this._data.owner;
    }

    get name() {
        return this._data.name;
    }

    set favorite(value) {
        // localstorage
        let favoritesList = [];
        if (localStorage.getItem("vg-favorites")) {
            favoritesList = localStorage.getItem(localStorageFavoriteTag).split("-").map(x => parseInt(x));
        }
        if (value && !favoritesList.includes(this.id)) {
            favoritesList.push(this.id);
            localStorage.setItem(localStorageFavoriteTag, favoritesList.join("-"));
            $(`#sailor-${this.id}`).addClass('favorite');
        } else if (!value && favoritesList.includes(this.id)) {
            favoritesList.splice(favoritesList.indexOf(this.id), 1);
            localStorage.setItem(localStorageFavoriteTag, favoritesList.join("-"));
            $(`#sailor-${this.id}`).removeClass('favorite');
        }

        //dashboard
        $(`#sailor-${this.id}`).find('.favorite-icon').attr('src', value ? toggleFavoriteOnSrc : toggleFavoriteOffSrc);
        if (this._positions) {
            if (this.visible) {
                this.hide();
                if (value) {
                    this.displayAsFavorite();
                } else {
                    this.displayAsStandard();
                }
            }
        } else {
            $.get(`../../backend/rest/get-tracker-vg.php?action=get-boat-trajectory&id=${this.id}`, onPositionsReceived);
        }
    }

    get favorite() {
        if (localStorage.getItem("vg-favorites")) {
            var favoritesList = localStorage.getItem("vg-favorites").split("-").map(x => parseInt(x));
            return favoritesList.includes(this.id);
        } else {
            return false;
        }
    }

    set visible(value) {
        if (this._visible == value) {
            return;
        }
        this._visible = value;
        // dashboard
        $(`#sailor-${this.id}`).find('.display-icon').attr('src', value ? toggleDisplayOnSrc : toggleDisplayOffSrc);

        if (this._positions) {
            if (value) {
                if (this.favorite) {
                    this.displayAsFavorite();
                } else {
                    this.displayAsStandard();
                }
            } else {
                this.hide();
            }
        } else {
            $.get(`../../backend/rest/get-tracker-vg.php?action=get-boat-trajectory&id=${this.id}`, onPositionsReceived);
        }
    }

    get visible() {
        return this._visible;
    }

    get positionsLoaded() {
        return this._positions != null;
    }
}

Boat.standardPathDuration = 10; // h
Boat.favoritePathDuration = 5 * 24; // h