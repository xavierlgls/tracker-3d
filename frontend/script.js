const boatAlt = 0;
const timeMultiplier = 1e4;
const topSize = 5;
const toggleFavoriteOnSrc = 'assets/star-fill.png';
const toggleFavoriteOffSrc = 'assets/star.png';
const toggleDisplayOnSrc = 'assets/eye.png';
const toggleDisplayOffSrc = 'assets/eye-slashed.png';
const toggleFleetAllSrc = 'assets/fleet_all.png';
const toggleFleetSelectionSrc = 'assets/fleet_selection.png';
const localStorageFavoriteTag = "vg-favorites";
const toggleFleetAllTitle = "display all the fleet";
const toggleFleetSelectionTitle = "display the selection only";

const course = [{
        "micnr": 33040,
        "miclon": -1.8386,
        "micname": "",
        "mictype": 1,
        "mictypename": "port startline buoy",
        "miclat": 46.4262
    },
    {
        "miclat": 46.4429,
        "mictype": 2,
        "mictypename": "starboard startline buoy",
        "micname": "",
        "micnr": 33041,
        "miclon": -1.8386
    },
    {
        "mictype": 6,
        "mictypename": "starboard gate buoy",
        "miclat": -60,
        "micnr": 33045,
        "miclon": 20,
        "micname": ""
    },
    {
        "miclat": -40,
        "mictypename": "port gate buoy",
        "mictype": 5,
        "micname": "",
        "miclon": 20,
        "micnr": 33044
    },
    {
        "miclat": -60,
        "mictypename": "starboard gate buoy",
        "mictype": 6,
        "micname": "",
        "miclon": 120,
        "micnr": 33047
    },
    {
        "miclon": 120,
        "micnr": 33046,
        "micname": "",
        "mictypename": "port gate buoy",
        "mictype": 5,
        "miclat": -40
    },
    {
        "micname": "",
        "miclon": -68.0163,
        "micnr": 33048,
        "miclat": -56.003,
        "mictypename": "racemark port round (counter clockwise)",
        "mictype": 4
    },
    {
        "mictype": 7,
        "mictypename": "port finishline buoy",
        "miclat": 46.4823,
        "micnr": 33042,
        "miclon": -1.8058,
        "micname": ""
    },
    {
        "mictypename": "starboard finishline buoy",
        "mictype": 8,
        "miclat": 46.4589,
        "miclon": -1.7845,
        "micnr": 33043,
        "micname": ""
    }
];

let lastUpdateTime = null; // unused yet
let startDate;
let stopDate;

let boats = [];

Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI1N2ViNGRmYi0wMzg0LTQ3NGYtOGJmYS0wMGFlNDIxNGQ3YzAiLCJpZCI6MjUzNzAwLCJpYXQiOjE3MzEwMjYwMDJ9.E_BY5WeChGNVCSQ019aX_oK1ASQeJjicLVDE6zt3ZmY';

const viewer = new Cesium.Viewer('cesiumContainer', {
    terrain: Cesium.Terrain.fromWorldTerrain(),
}); 

course.forEach(item => {
    viewer.entities.add({
        position: Cesium.Cartesian3.fromDegrees(item.miclon, item.miclat, 0),
        point: { pixelSize: 10, color: Cesium.Color.ORANGE }
      }); 
});


// mouse events handlers

var pickedEntities = new Cesium.EntityCollection();
var pickColor = Cesium.Color.AQUA.withAlpha(0.6);
var pickFavoriteColor = Cesium.Color.RED.withAlpha(0.6);

function makeStandardProperty(entity, color) {
    var colorProperty = new Cesium.CallbackProperty(function (time, result) {
            if (pickedEntities.contains(entity)) {
                return pickColor.clone(result);
            }
            return color.clone(result);
        },
        false);

    entity.polyline.material = new Cesium.ColorMaterialProperty(
        colorProperty
    );
}

function makeFavoriteProperty(entity, color) {
    var colorProperty = new Cesium.CallbackProperty(function (time, result) {
            if (pickedEntities.contains(entity)) {
                return pickFavoriteColor.clone(result);
            }
            return color.clone(result);
        },
        false);

    entity.polyline.material = new Cesium.ColorMaterialProperty(
        colorProperty
    );
}

var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
handler.setInputAction(function (movement) {
    // get an array of all primitives at the mouse position
    var pickedObjects = viewer.scene.drillPick(movement.endPosition);
    if (Cesium.defined(pickedObjects)) {
        //Update the collection of picked entities.
        pickedEntities.removeAll();
        for (var i = 0; i < pickedObjects.length; ++i) {
            var entity = pickedObjects[i].id;
            pickedEntities.add(entity);
        }
    }
}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

$(document).ready(function () {
    $.get('../../backend/rest/get-tracker-vg.php?action=get-race-status', onRaceStatus);
    $(".footer").css("position", "relative");
});

function onRaceStatus(result) {
    if (result.success) {
        startDate = Cesium.JulianDate.fromDate(new Date(Date.parse(result.race.start_date)));
        stopDate = Cesium.JulianDate.fromDate(new Date(Date.parse(result.race.last_date)));
        viewer.clock.startTime = startDate.clone();
        viewer.clock.stopTime = stopDate.clone();
        viewer.clock.currentTime = stopDate.clone();
        viewer.clock.multiplier = timeMultiplier;
        viewer.clock.clockRange = Cesium.ClockRange.CLAMPED;
        viewer.timeline.zoomTo(startDate, stopDate);

        $.get('../../backend/rest/get-tracker-vg.php?action=get-leaderboard', onLeaderBoard);
    } else {
        console.error(result.message);
    }
}

function onLeaderBoard(result) {
    if (result.success) {
        const sailors = result.sailors
            .filter(function (x) {
                return parseInt(x.rank) != 0;
            })
            .sort(function (a, b) {
                return parseInt(a.rank) - parseInt(b.rank);
            });
        sailors.forEach(sailor => {
            boats[sailor.id] = new Boat(sailor);
            if (boats[sailor.id].rank <= topSize || boats[sailor.id].favorite) {
                boats[sailor.id].visible = true;
            }
        });
        const select = $('select[name="player-selector"]');
        sailors.forEach(sailor => {
            const option = new Option(sailor.skipper, sailor.id);
            select.append($('<option>', {
                value: sailor.id,
                text: `${boats[sailor.id].rank} - ${decodeURI(boats[sailor.id].skipper)}`
            }));
        });
        initFilter();
        select.change(function () {
            $.get(`../../backend/rest/get-tracker-vg.php?action=get-race-stats&id=${select.val()}`, onRaceStats);
        });
        setTimeout(function () {
            viewer.flyTo(viewer.entities);
        }, 1000);
        if (typeof initChart === "function") {
            initChart();
        }
    } else {
        console.error(result.message);
    }
}

function initFilter() {
    const filter = $('input[name="sailor-filter"]');
    filter.on('input', function () {
        $('#sailors-container .sailor-container').each(function () {
            const skipperName = $(this).find('span.skipper:first').text();
            if (skipperName.toLowerCase().includes(filter.val().toLowerCase())) {
                $(this).css('display', 'flex');
            } else {
                $(this).css('display', 'none');
            }
        });
    });
}

function onToggleDisplay(id) {
    boats[id].visible = !boats[id].visible;
}

function onToggleFavorite(id) {
    boats[id].favorite = !boats[id].favorite;
}

function goToSailor(id) {
    boats[id].flyTo();
}

function onPositionsReceived(result) {
    boats[result.id].onPositions(result.positions);
}

function displayAllTheFleet() {
    let delay = 0;
    boats.forEach(boat => {
        if (boat.positionsLoaded) {
            boat.fleetMode = true;
        } else {
            setTimeout(() => {
                boat.fleetMode = true;
            }, delay);
            delay += 100;
        }
    });
}

function displayTheSelectionOnly() {
    boats.forEach(boat => {
        boat.fleetMode = false;
    });
}

function dashboardToggle() {
    if ($('#dash-header').css('display') == 'flex') {
        $('#dash-header').css('display', 'none');
        $('#sailors-container').css('display', 'none');
    } else {
        $('#dash-header').css('display', 'flex');
        $('#sailors-container').css('display', 'block');
    }
}

function fleetModeToggle() {
    if ($('#fleet-toggle').attr('src') == toggleFleetAllSrc) {
        $('#fleet-toggle').attr('src', toggleFleetSelectionSrc);
        $('#fleet-toggle').attr('title', toggleFleetAllTitle);
        displayTheSelectionOnly();
    } else if ($('#fleet-toggle').attr('src') == toggleFleetSelectionSrc) {
        $('#fleet-toggle').attr('src', toggleFleetAllSrc);
        $('#fleet-toggle').attr('title', toggleFleetSelectionTitle);
        displayAllTheFleet();
    }
}