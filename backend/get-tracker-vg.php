<?php
require_once dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . 'config_v2.php';
require_once dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'VgManager.php';

header('Content-Type: application/json');

$vgManager = new VgManager();

$return;
$return["success"] = false;

switch ($_GET["action"]) {
    case 'get-boat-trajectory':
        getTrajectory();
        break;
    case 'get-leaderboard':
        getLeaderBoard();
        break;
    case 'get-race-status':
        getRaceStatus();
        break;
    case 'get-race-stats':
        getRaceStats();
        break;
    case 'update-rankings':
        updateRankings();
        break;
    case 'get-rankings':
        getRankings();
        break;
    case 'test':
        test();
        break;
    default:
        $return["message"] = "The action is not correct";
        $return["success"] = false;
        break;
}

echo json_encode($return);

function getLeaderBoard()
{
    global $return, $vgManager;

    if (true) {
        try {
            $return["sailors"] = $vgManager->getLeaderBoard();
            $return["success"] = true;
        } catch (Exception $e) {
            $return["success"] = false;
            $return["message"] = $e->getMessage();
        }
    } else {
        $return["success"] = false;
        $return["message"] = "Not enough information in the request";
    }
}

function getTrajectory()
{
    global $return, $vgManager;

    if (isset($_GET["id"])) {
        try {
            $id = intval($_GET["id"]);
            $return["id"] = $id;
            $return["positions"] = $vgManager->getTrajectory($id);
            $return["success"] = true;
        } catch (Exception $e) {
            $return["success"] = false;
            $return["message"] = $e->getMessage();
        }
    } else {
        $return["success"] = false;
        $return["message"] = "Not enough information in the request";
    }
}

function getRaceStatus()
{
    global $return, $vgManager;

    if (true) {
        try {
            $return["race"] = $vgManager->getRaceStatus();
            $return["success"] = true;
        } catch (Exception $e) {
            $return["success"] = false;
            $return["message"] = $e->getMessage();
        }
    } else {
        $return["success"] = false;
        $return["message"] = "Not enough information in the request";
    }
}

function getRaceStats()
{
    global $return, $vgManager;

    if (isset($_GET["id"])) {
        try {
            $id = intval($_GET["id"]);
            $return["stats"] = $vgManager->getRaceStats($id);
            $return["id"] = $id;
            $return["success"] = true;
        } catch (Exception $e) {
            $return["success"] = false;
            $return["message"] = $e->getMessage();
        }
    } else {
        $return["success"] = false;
        $return["message"] = "Not enough information in the request";
    }
}

function getRankings()
{
    global $return, $vgManager;

    try {
        $return["boats"] = $vgManager->getLeaderBoard();

        $return["success"] = true;
    } catch (Exception $e) {
        $return["success"] = false;
        $return["message"] = $e->getMessage();
    }
}

function updateRankings()
{
    global $return, $vgManager;

    try {
        $vgManager->updateRankings();

        $return["success"] = true;
    } catch (Exception $e) {
        $return["success"] = false;
        $return["message"] = $e->getMessage();
    }
}

function test()
{
    global $return, $vgManager;

    try {
        $vgManager->computeRanks();

        $return["success"] = true;
    } catch (Exception $e) {
        $return["success"] = false;
        $return["message"] = $e->getMessage();
    }
}
