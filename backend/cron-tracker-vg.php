<?php
require_once dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . 'config_v2.php';
require_once dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'VgManager.php';
require_once dirname(__DIR__, 1) . DIRECTORY_SEPARATOR . 'simple_html_dom.php';

$url_sa3_auto = "https://sailaway.world/cgi-bin/sailaway3/page.pl?p=event&nm=vg-2024";
$url_sa3_manu = "https://sailaway.world/cgi-bin/sailaway3/page.pl?p=event&nm=vg-2024-1";
$url_sa2_auto = "https://sailaway.world/cgi-bin/sailaway3/page.pl?p=event&nm=sa2-le-vendee-globe-2024";
$url_vg = "https://www.geovoile.net/vendeeglobe/public/posreport/?key=cb7e0f292e164c4aa2d6dc4abfaa1771";

$classes = array();
$classes["SA2-auto"] = 0;
$classes["SA3-auto"] = 1;
$classes["SA3-manu"] = 2;
$classes["real"]     = 3;

function getDB()
{
    global $DB_NAME;
    global $DB_USERNAME;
    global $DB_HOST;
    global $DB_PASSWORD;
    $db = new PDO('mysql:dbname=' . $DB_NAME . ';host=' . $DB_HOST, $DB_USERNAME, $DB_PASSWORD);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    return $db;
}

function posreport_vg_scraping($url) {
    global $classes;

    echo "<h1>Vendee Globe</h1>";

    function convertToSqlDatetime($dateString) {
        // On utilise la méthode createFromFormat pour interpréter le format "m/d/y H:i"
        $date = DateTime::createFromFormat('m/d/y H:i', $dateString);
    
        // Vérifie si la conversion est réussie
        if ($date === false) {
            return null; // Retourne null si le format est incorrect
        }
    
        // Retourne la date au format SQL "Y-m-d H:i:s"
        return $date->format('Y-m-d H:i:s');
    }

    try {
        $db = getDB();
        $boats = $db->query("SELECT * FROM vg2024_boats WHERE class = " . $classes["real"])->fetchAll();

        $lastPos = $db->query("SELECT boats.id, pos.date, boats.owner_id FROM vg2024_positions AS pos JOIN vg2024_boats AS boats ON pos.boat_id = boats.id WHERE pos.date = (SELECT MAX(pos2.date) FROM vg2024_positions AS pos2 WHERE pos2.boat_id = pos.boat_id AND boats.class = " . $classes["real"] . ")")->fetchAll();

        # Setup
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        # Scrapping
        $csvContent = curl_exec($curl);

        if ($csvContent === false) {
            echo "Error : cannot scrape " . $url;
            return 0;
        }
        curl_close($curl);

        $csvContent = str_replace("POSREPORT", "", $csvContent);
        $csvItems = explode(";", $csvContent);

        $partials = [];
        for($csvIndex = 0; $csvIndex < 40*5; $csvIndex += 5) {
            $vgId = intval($csvItems[$csvIndex]);
            $latStr = $csvItems[$csvIndex + 2];
            $lat = null;
            if (preg_match('/([\d\.]+)([NS])/', $latStr, $matches)) {
                $lat = ($matches[2] == "N" ? 1 : -1) * (floatval($matches[1]));
            }
            $lngStr = $csvItems[$csvIndex + 3];
            $lng = null;
            if (preg_match('/([\d\.]+)([EW])/', $lngStr, $matches)) {
                $lng = ($matches[2] == "E" ? 1 : -1) * (floatval($matches[1]));
            }
            $dateStr = $csvItems[$csvIndex + 4];
            $date = convertToSqlDatetime($dateStr);

            $boatId = array_values(array_filter($boats, function($item) use ($vgId) {
                return $item->owner_id == $vgId;
            }))[0]->id;

            $lastUpdateDate = "1970-01-10 10:10:10";
            $lastUpdateDateArray = array_values(array_filter($lastPos, function($item) use ($vgId) {
                return $item->owner_id == $vgId;
            }));
            if (count($lastUpdateDateArray) > 0) {
                $lastUpdateDate = $lastUpdateDateArray[0]->date;
            }

            $lastDateDateTime = new DateTime($lastUpdateDate);
            $dateDateTime = new DateTime($date);

            if (!is_null($lat) && !is_null($lng) && !is_null($date) && ($dateDateTime > $lastDateDateTime)) {
                $partial = "(";
                $partial .= ($boatId . ", ");
                $partial .= ($lat . ", ");
                $partial .= ($lng . ", ");
                $partial .= (0 . ", ");
                $partial .= (0 . ", ");
                $partial .= (0 . ", ");
                $partial .= $db->quote($date);
                $partial .= ")";
                $partials[] = $partial;
            }
            
        }

        if (count($partials) > 0) {
            $sql = "INSERT INTO vg2024_positions (boat_id, lat, lng, rank, heading, speed, date) VALUES ";
            $sql .= join(", ", $partials);
            $db->query($sql);
            echo "<p>" . count($partials) . " position" . (count($partials) > 1 ? "s" : "") . " added</p>";
        } else {
            echo "<p>No position added</p>";
        }

    } catch (\Throwable $th) {
        echo $th->getMessage();
    }

}

function race_sa_scraping($url) {

    $output = [];

    try {

        # Setup
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        # Scrapping
        $htmlContent = curl_exec($curl);

        if ($htmlContent === false) {
            echo "Error : cannot scrape " . $url;
            return 0;
        }
        curl_close($curl);

        # Parsing
        $html = str_get_html($htmlContent);
        $lines = $html->find("table")[3]->find("tr");

        $rank = 1;

        foreach (array_slice($lines, 1) as $line) {
            $object = new stdClass();
            $owner_id = null;
            $idStr = $line->find("td")[2]->find("a")[0]->outertext();
            if (preg_match('/&nr=(\d+)"/', $idStr, $matches)) {
                $owner_id = intval($matches[1]);
            }
            $skipper = $line->find("td")[2]->find("a")[0]->innerText();
            $boat = $line->find("td")[3]->find("p")[0]->innerText();
            $locationStr = $line->find("td")[4]->innerText();
            $hdg = null;
            $speed = null;
            $lat = null;
            $lng = null;
            if (preg_match('/([NS])(\d+).*? ([\d\.]+)\' ([EW])(\d+).*? ([\d\.]+)\', HDG (\d+).*?, SPD ([\d\. ]+)kn/', $locationStr, $matches)) {
                $speed = floatval($matches[8]);
                $hdg = intval($matches[7]);
                $lat = ($matches[1] == "N" ? 1 : -1) * (floatval($matches[2]) + floatval($matches[3]) / 60.0);
                $lng = ($matches[4] == "E" ? 1 : -1) * (floatval($matches[5]) + floatval($matches[6]) / 60.0);
            }
            $object->owner_id = $owner_id;
            $object->boat = $boat;
            $object->skipper = $skipper;
            $object->hdg = $hdg;
            $object->speed = $speed;
            $object->lat = $lat;
            $object->lng = $lng;
            $object->rank = $rank;

            $rank += 1;

            $output[] = $object;
        }

    } catch (\Throwable $th) {
        echo $th->getMessage();
    }

    return $output;
}

function update_sa_positions($scraped, $className) {
    global $classes;
    echo "<h1>Sailaway : " . $className . "</h1>";
    try {
        $db = getDB();
        $existingBoats = $db->query("SELECT * FROM vg2024_boats WHERE class = " . $classes[$className])->fetchAll();
        $lastPos = $db->query("SELECT boats.id, pos.date, boats.owner_id FROM vg2024_positions AS pos JOIN vg2024_boats AS boats ON pos.boat_id = boats.id WHERE pos.date = (SELECT MAX(pos2.date) FROM vg2024_positions AS pos2 WHERE pos2.boat_id = pos.boat_id AND boats.class = " . $classes[$className] . ")")->fetchAll();
        
        // Ajout des nouveaux bateaux
        
        $boatsAdded = [];
        $existingOwnerIds = array_map(function ($item) {
            return intval($item->owner_id);
        }, $existingBoats);
        $sql = "INSERT INTO vg2024_boats (owner, owner_id, name, class, rank) VALUES ";
        $partials = [];
        foreach ($scraped as $pos) {
            if (!in_array($pos->owner_id, $existingOwnerIds)) {
                $partials[] = "(" . $db->quote($pos->skipper) . ", " . $pos->owner_id . ", " . $db->quote($pos->boat) . ", " . $classes[$className] . ", " . $pos->rank . ")";
                $boatsAdded[] = $pos->skipper;
            }
        }
        if (count($boatsAdded) > 0) {
            $sql .= join(", ", $partials);
            $db->query($sql);
            echo "<p>" . count($boatsAdded) . " boat" . (count($boatsAdded) > 1 ? "s" : "") . " added : " . join(", ", $boatsAdded) . "</p>";
        } else {
            echo "<p>No boat added</p>";
        }

        $existingBoats = $db->query("SELECT * FROM vg2024_boats WHERE class = " . $classes[$className])->fetchAll();
    
        // Mise a jour des classements
    
        $boatsUpdated = [];
        foreach ($scraped as $pos) {
            $previousPos = array_values(array_filter($existingBoats, function($item) use ($pos) {
                return $item->owner_id == $pos->owner_id;
            }))[0];
            if (($previousPos->rank != $pos->rank) || ($previousPos->owner != $pos->skipper) || ($previousPos->name != $pos->boat)) {
                $sql = "UPDATE vg2024_boats SET owner=" . $db->quote($pos->skipper) . ", name=" . $db->quote($pos->boat) . ", rank=" . $pos->rank . " WHERE owner_id = " . $pos->owner_id;
                $db->query($sql);
                $boatsUpdated[] = $pos->skipper;
            }
        }
    
        if (count($boatsUpdated) > 0) {
            echo "<p>" . count($boatsUpdated) . " boat" . (count($boatsUpdated) > 1 ? "s" : "") . " updated : " . join(", ", $boatsUpdated) . "</p>";
        } else {
            echo "<p>No boat updated</p>";
        }
    
    
        // Ajout des nouvelles positions
    
        $partials = [];
        foreach ($scraped as $pos) {
            
            // Securite : on verifie que la precedente position n'est pas trop recente
            $lastUpdateDate = "1970-01-10 10:10:10";
            $lastUpdateDateArray = array_values(array_filter($lastPos, function($item) use ($pos) {
                return $item->owner_id ==$pos->owner_id;
            }));
            if (count($lastUpdateDateArray) > 0) {
                $lastUpdateDate = $lastUpdateDateArray[0]->date;
            }
            $lastDateDateTime = new DateTime($lastUpdateDate);
            $currentDateTime = new DateTime();
            $lastDateDateTime->add(new DateInterval('PT10M'));
            $oldEnough = ($currentDateTime > $lastDateDateTime);

            if (!is_null($pos->lat) && !is_null($pos->lng) && !is_null($pos->speed) && !is_null($pos->hdg) && $oldEnough) {
                $correspondingBoatId = array_values(array_filter($existingBoats, function($item) use ($pos) {
                    return $item->owner_id == $pos->owner_id;
                }))[0]->id;
                $partial = "(";
                $partial .= ($correspondingBoatId . ", ");
                $partial .= ($pos->lat . ", ");
                $partial .= ($pos->lng . ", ");
                $partial .= ($pos->rank . ", ");
                $partial .= ($pos->hdg . ", ");
                $partial .= $pos->speed;
                $partial .= ")";
                $partials[] = $partial;
            }
        }

        if (count($partials) > 0) {
            $sql = "INSERT INTO vg2024_positions (boat_id, lat, lng, rank, heading, speed) VALUES ";
            $sql .= join(", ", $partials);
            $db->query($sql);
            echo "<p>" . count($partials) . " position" . (count($partials) > 1 ? "s" : "") . " added</p>";
        } else {
            echo "<p>No position added</p>";
        }
    
    } catch (\Throwable $th) {
        echo $th->getMessage();
    }
}

posreport_vg_scraping($url_vg);
update_sa_positions(race_sa_scraping($url_sa3_auto), "SA3-auto");
update_sa_positions(race_sa_scraping($url_sa3_manu), "SA3-manu");
update_sa_positions(race_sa_scraping($url_sa2_auto), "SA2-auto");