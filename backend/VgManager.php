<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'DBManager.php';
require_once dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'config_v2.php';

class VgManager extends DBManager
{
    private static $DATE_FORMAT = "Y-m-d G:i:s";

    public function __construct()
    {
        parent::__construct();
    }

    public function getLeaderBoard(): array
    {
        return $this->db->query("SELECT * FROM vg_boats")->fetchAll();
    }

    public function getRaceStatus()
    {
        $output = [];
        $output["start_date"] = ($this->db->query("SELECT MIN(date) AS earliestDate FROM vg_positions")->fetch())->earliestDate;
        $output["last_date"] = ($this->db->query("SELECT MAX(date) AS lastDate FROM vg_positions")->fetch())->lastDate;
        return $output;
    }

    public function getTrajectory(int $id): array
    {
        return $this->db->query("SELECT date, lat, lng, rank, starboard, heading FROM vg_positions WHERE boat_id=$id ORDER BY date DESC")->fetchAll();
    }

    public function getRaceStats(int $id): array
    {
        return $this->db->query("SELECT date, rank, heading, wind_direction, wind_speed, speed FROM vg_positions WHERE boat_id=$id ORDER BY date")->fetchAll();
    }

    public function updateRankings(): void
    {
        $skippers = $this->getLeaderBoard();

        foreach ($skippers as $skipper) {
            $continue = false;
            if ($skipper->date_equatorDown === NULL && !$continue) {
                $positions = $this->db->query("SELECT lat, lng, date FROM vg_positions WHERE boat_id=$skipper->id AND lat > -1 AND lat < 1 ORDER BY date")->fetchAll();
                $timestamp = $this->getEquatorDownDate($positions);
                if ($timestamp !== NULL) {
                    $date = date(self::$DATE_FORMAT, $timestamp);
                    $this->db->query("UPDATE vg_boats SET date_equatorDown=" . $this->db->quote($date) . " WHERE id=$skipper->id");
                } else {
                    $continue = true;
                }
            }
            if ($skipper->date_capeOfGoodHope === NULL && !$continue) {
                $positions = $this->db->query("SELECT lat, lng, date FROM vg_positions WHERE boat_id=$skipper->id AND lng > 17.5 AND lng < 19.5 AND lat < -32 ORDER BY date")->fetchAll();
                $timestamp = $this->getCapeOfGoodHopeDate($positions);
                if ($timestamp !== NULL) {
                    $date = date(self::$DATE_FORMAT, $timestamp);
                    $this->db->query("UPDATE vg_boats SET date_capeOfGoodHope=" . $this->db->quote($date) . " WHERE id=$skipper->id");
                } else {
                    $continue = true;
                }
            }
            if ($skipper->date_capeLeeuwin === NULL && !$continue) {
                $positions = $this->db->query("SELECT lat, lng, date FROM vg_positions WHERE boat_id=$skipper->id AND lng > 114 AND lng < 116 AND lat < -32 ORDER BY date")->fetchAll();
                $timestamp = $this->getCapeLeeuwinDate($positions);
                if ($timestamp !== NULL) {
                    $date = date(self::$DATE_FORMAT, $timestamp);
                    $this->db->query("UPDATE vg_boats SET date_capeLeeuwin=" . $this->db->quote($date) . " WHERE id=$skipper->id");
                } else {
                    $continue = true;
                }
            }
            if ($skipper->date_antimeridial === NULL && !$continue) {
                $positions = $this->db->query("SELECT lat, lng, date FROM vg_positions WHERE boat_id=$skipper->id AND (lng > 179 OR lng < -179) AND lat < -32 ORDER BY date")->fetchAll();
                $timestamp = $this->getAntimeridialDate($positions);
                if ($timestamp !== NULL) {
                    $date = date(self::$DATE_FORMAT, $timestamp);
                    $this->db->query("UPDATE vg_boats SET date_antimeridial=" . $this->db->quote($date) . " WHERE id=$skipper->id");
                } else {
                    $continue = true;
                }
            }
            if ($skipper->date_capeHorn === NULL && !$continue) {
                $positions = $this->db->query("SELECT lat, lng, date FROM vg_positions WHERE boat_id=$skipper->id AND lng > -68 AND lng < -66 AND lat < -32 ORDER BY date")->fetchAll();
                $timestamp = $this->getCapeHornDate($positions);
                if ($timestamp !== NULL) {
                    $date = date(self::$DATE_FORMAT, $timestamp);
                    $this->db->query("UPDATE vg_boats SET date_capeHorn=" . $this->db->quote($date) . " WHERE id=$skipper->id");
                } else {
                    $continue = true;
                }
            }
            if ($skipper->date_equatorUp === NULL && !$continue) {
                $limitDate = (new DateTime($skipper->date_equatorDown))->add(new DateInterval("P3W"));
                $positions = $this->db->query("SELECT lat, lng, date FROM vg_positions WHERE boat_id=$skipper->id AND lat > -1 AND lat < 1 AND date > " . $this->db->quote($limitDate->format("Y-m-d H:i:s")) . " ORDER BY date")->fetchAll();
                if($skipper->id == 152244){
                    foreach($positions as $pos){
                        var_dump($pos);
                    }
                }
                $timestamp = $this->getEquatorUpDate($positions);
                if ($timestamp !== NULL) {
                    $date = date(self::$DATE_FORMAT, $timestamp);
                    $this->db->query("UPDATE vg_boats SET date_equatorUp=" . $this->db->quote($date) . " WHERE id=$skipper->id");
                } else {
                    $continue = true;
                }
            }
        }
    }

    public function computeRanks(): void
    {
        $rank = 1;

        // finished
        $boats = $this->db->query("SELECT id FROM vg_boats WHERE date_finish IS NOT NULL ORDER BY date_finish")->fetchAll();
        foreach ($boats as $boat) {
            $this->db->query("UPDATE SET computed_rank=$rank, distance_to_leader=NULL WHERE id=$boat->id");
            $rank++;
        }

        // equator back to finish
        $boats = $this->db->query("SELECT id FROM vg_boats WHERE date_equatorUp IS NOT NULL AND date_finish IS NULL")->fetchAll();
        $temp = [];
        foreach ($boats as $boat) {
            $temp[] = $this->db->query("SELECT * FROM `vg_boats` INNER JOIN `vg_positions` ON vg_positions.boat_id=vg_boats.id WHERE vg_boats.id=$boat->id ORDER BY vg_positions.date DESC LIMIT 1")->fetch();
        }
        $nextLat = 46.4798;
        $nextLng = -1.7927;
        $temp = [];
        foreach ($boats as $boat) {
            $temp[] = $this->db->query("SELECT vg_boats.id AS id, vg_positions.lat AS lat, vg_positions.lng AS lng FROM vg_boats INNER JOIN vg_positions ON vg_positions.boat_id=vg_boats.id WHERE vg_boats.id=$boat->id ORDER BY vg_positions.date DESC LIMIT 1")->fetch();
        }
        usort($temp, function ($a, $b) use ($nextLat, $nextLng) {
            $distA = DBManager::distanceStat($a->lat, $a->lng, $nextLat, $nextLng, "N");
            $distB = DBManager::distanceStat($b->lat, $b->lng, $nextLat, $nextLng, "N");
            return $distA - $distB;
        });
        foreach ($temp as $item) {
            $this->db->query("UPDATE vg_boats SET computed_rank=$rank WHERE id=$item->id");
            $rank++;
        }

        // horn to equator back
        $boats = $this->db->query("SELECT * FROM vg_boats WHERE date_capeHorn IS NOT NULL AND date_equatorUp IS NULL")->fetchAll();
        $nextLat = 3;
        $nextLng = -28;
        $temp = [];
        foreach ($boats as $boat) {
            $temp[] = $this->db->query("SELECT vg_boats.id AS id, vg_positions.lat AS lat, vg_positions.lng AS lng FROM vg_boats INNER JOIN vg_positions ON vg_positions.boat_id=vg_boats.id WHERE vg_boats.id=$boat->id ORDER BY vg_positions.date DESC LIMIT 1")->fetch();
        }
        usort($temp, function ($a, $b) use ($nextLat, $nextLng) {
            $distA = DBManager::distanceStat($a->lat, $a->lng, $nextLat, $nextLng, "N");
            $distB = DBManager::distanceStat($b->lat, $b->lng, $nextLat, $nextLng, "N");
            return $distA - $distB;
        });
        foreach ($temp as $item) {
            $this->db->query("UPDATE vg_boats SET computed_rank=$rank WHERE id=$item->id");
            $rank++;
        }

        // antimeridial to horn
        $boats = $this->db->query("SELECT * FROM vg_boats WHERE date_antimeridial IS NOT NULL AND date_capeHorn IS NULL")->fetchAll();
        $nextLat = -55;
        $nextLng = -63;
        $temp = [];
        foreach ($boats as $boat) {
            $temp[] = $this->db->query("SELECT vg_boats.id AS id, vg_positions.lat AS lat, vg_positions.lng AS lng FROM vg_boats INNER JOIN vg_positions ON vg_positions.boat_id=vg_boats.id WHERE vg_boats.id=$boat->id ORDER BY vg_positions.date DESC LIMIT 1")->fetch();
        }
        usort($temp, function ($a, $b) use ($nextLat, $nextLng) {
            $distA = DBManager::distanceStat($a->lat, $a->lng, $nextLat, $nextLng, "N");
            $distB = DBManager::distanceStat($b->lat, $b->lng, $nextLat, $nextLng, "N");
            return $distA - $distB;
        });
        foreach ($temp as $item) {
            $this->db->query("UPDATE vg_boats SET computed_rank=$rank WHERE id=$item->id");
            $rank++;
        }

        // leeuwin to antimeridial
        $boats = $this->db->query("SELECT * FROM vg_boats WHERE date_capeLeeuwin IS NOT NULL AND date_antimeridial IS NULL")->fetchAll();
        $nextLat = -50;
        $nextLng = -175;
        $temp = [];
        foreach ($boats as $boat) {
            $temp[] = $this->db->query("SELECT vg_boats.id AS id, vg_positions.lat AS lat, vg_positions.lng AS lng FROM vg_boats INNER JOIN vg_positions ON vg_positions.boat_id=vg_boats.id WHERE vg_boats.id=$boat->id ORDER BY vg_positions.date DESC LIMIT 1")->fetch();
        }
        usort($temp, function ($a, $b) use ($nextLat, $nextLng) {
            $distA = DBManager::distanceStat($a->lat, $a->lng, $nextLat, $nextLng, "N");
            $distB = DBManager::distanceStat($b->lat, $b->lng, $nextLat, $nextLng, "N");
            return $distA - $distB;
        });
        foreach ($temp as $item) {
            $this->db->query("UPDATE vg_boats SET computed_rank=$rank WHERE id=$item->id");
            $rank++;
        }

        // good hope to leeuwin
        $boats = $this->db->query("SELECT * FROM vg_boats WHERE date_capeOfGoodHope IS NOT NULL AND date_capeLeeuwin IS NULL")->fetchAll();
        $nextLat = -50;
        $nextLng = 120;
        $temp = [];
        foreach ($boats as $boat) {
            $temp[] = $this->db->query("SELECT vg_boats.id AS id, vg_positions.lat AS lat, vg_positions.lng AS lng FROM vg_boats INNER JOIN vg_positions ON vg_positions.boat_id=vg_boats.id WHERE vg_boats.id=$boat->id ORDER BY vg_positions.date DESC LIMIT 1")->fetch();
        }
        usort($temp, function ($a, $b) use ($nextLat, $nextLng) {
            $distA = DBManager::distanceStat($a->lat, $a->lng, $nextLat, $nextLng, "N");
            $distB = DBManager::distanceStat($b->lat, $b->lng, $nextLat, $nextLng, "N");
            return $distA - $distB;
        });
        foreach ($temp as $item) {
            $this->db->query("UPDATE vg_boats SET computed_rank=$rank WHERE id=$item->id");
            $rank++;
        }

        // equator to good hope
        $boats = $this->db->query("SELECT * FROM vg_boats WHERE date_equatorDown IS NOT NULL AND date_capeOfGoodHope IS NULL")->fetchAll();
        $nextLat = -40;
        $nextLng = 20;
        $temp = [];
        foreach ($boats as $boat) {
            $temp[] = $this->db->query("SELECT vg_boats.id AS id, vg_positions.lat AS lat, vg_positions.lng AS lng FROM vg_boats INNER JOIN vg_positions ON vg_positions.boat_id=vg_boats.id WHERE vg_boats.id=$boat->id ORDER BY vg_positions.date DESC LIMIT 1")->fetch();
        }
        usort($temp, function ($a, $b) use ($nextLat, $nextLng) {
            $distA = DBManager::distanceStat($a->lat, $a->lng, $nextLat, $nextLng, "N");
            $distB = DBManager::distanceStat($b->lat, $b->lng, $nextLat, $nextLng, "N");
            return $distA - $distB;
        });
        foreach ($temp as $item) {
            $this->db->query("UPDATE vg_boats SET computed_rank=$rank WHERE id=$item->id");
            $rank++;
        }

        // to equator
        $boats = $this->db->query("SELECT * FROM vg_boats WHERE date_equatorDown IS NULL")->fetchAll();
        $nextLat = -0.5;
        $nextLng = -27.5;
        $temp = [];
        foreach ($boats as $boat) {
            $output = $this->db->query("SELECT vg_boats.id AS id, vg_positions.lat AS lat, vg_positions.lng AS lng FROM vg_boats INNER JOIN vg_positions ON vg_positions.boat_id=vg_boats.id WHERE vg_boats.id=$boat->id ORDER BY vg_positions.date DESC LIMIT 1")->fetch();
            if ($output->lat) {
                $temp[] = $output;
            }
        }
        usort($temp, function ($a, $b) use ($nextLat, $nextLng) {
            $distA = DBManager::distanceStat($a->lat, $a->lng, $nextLat, $nextLng, "N");
            $distB = DBManager::distanceStat($b->lat, $b->lng, $nextLat, $nextLng, "N");
            return $distA - $distB;
        });
        foreach ($temp as $item) {
            $this->db->query("UPDATE vg_boats SET computed_rank=$rank WHERE id=$item->id");
            $rank++;
        }
    }


    // returns timestamp or null if not passed yet
    private function getEquatorDownDate($positions)
    {
        $latRef = 0;
        $index = 1;
        while ($index < sizeof($positions)) {
            $currentPosition = $positions[$index];
            if ($latRef > $currentPosition->lat) {
                $previousPosition = $positions[$index - 1];
                $previousTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $previousPosition->date)->getTimestamp();
                $currentTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
                $dLat = abs($currentPosition->lat  - $previousPosition->lat);
                $dDate = $currentTimeStamp - $previousTimeStamp;
                return intval(($previousPosition->lat - $latRef) / $dLat * $dDate + $previousTimeStamp);
            } else if ($latRef == $currentPosition->lat) {
                return DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
            }
            $index++;
        }
        return null;
    }

    // returns timestamp or null if not passed yet
    private function getCapeOfGoodHopeDate($positions)
    {
        $lngRef = 18.473;
        $index = 1;
        while ($index < sizeof($positions)) {
            $currentPosition = $positions[$index];
            if ($lngRef < $currentPosition->lng) {
                $previousPosition = $positions[$index - 1];
                $previousTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $previousPosition->date)->getTimestamp();
                $currentTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
                $dLng = abs($currentPosition->lng  - $previousPosition->lng);
                $dDate = $currentTimeStamp - $previousTimeStamp;
                return intval(($lngRef - $previousPosition->lng) / $dLng * $dDate + $previousTimeStamp);
            } else if ($lngRef == $currentPosition->lng) {
                return DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
            }
            $index++;
        }
        return null;
    }

    // returns timestamp or null if not passed yet
    private function getCapeLeeuwinDate($positions)
    {
        $lngRef = 115.1313;
        $index = 1;
        while ($index < sizeof($positions)) {
            $currentPosition = $positions[$index];
            if ($lngRef < $currentPosition->lng) {
                $previousPosition = $positions[$index - 1];
                $previousTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $previousPosition->date)->getTimestamp();
                $currentTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
                $dLng = abs($currentPosition->lng  - $previousPosition->lng);
                $dDate = $currentTimeStamp - $previousTimeStamp;
                return intval(($lngRef - $previousPosition->lng) / $dLng * $dDate + $previousTimeStamp);
            } else if ($lngRef == $currentPosition->lng) {
                return DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
            }
            $index++;
        }
        return null;
    }

    // returns timestamp or null if not passed yet
    private function getAntimeridialDate($positions)
    {
        $index = 1;
        while ($index < sizeof($positions)) {
            $currentPosition = $positions[$index];
            if (0 > $currentPosition->lng) {
                $previousPosition = $positions[$index - 1];
                $previousTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $previousPosition->date)->getTimestamp();
                $currentTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
                $dLng = 180 + $currentPosition->lng + 180 - $previousPosition->lng;
                $dDate = $currentTimeStamp - $previousTimeStamp;
                return intval((180 - $previousPosition->lng) / $dLng * $dDate + $previousTimeStamp);
            } else if (180 == $currentPosition->lng) {
                return DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
            }
            $index++;
        }
        return null;
    }

    // returns timestamp or null if not passed yet
    private function getCapeHornDate($positions)
    {
        $lngRef = -67.289;
        $index = 1;
        while ($index < sizeof($positions)) {
            $currentPosition = $positions[$index];
            if ($lngRef < $currentPosition->lng) {
                $previousPosition = $positions[$index - 1];
                $previousTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $previousPosition->date)->getTimestamp();
                $currentTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
                $dLng = abs($currentPosition->lng  - $previousPosition->lng);
                $dDate = $currentTimeStamp - $previousTimeStamp;
                return intval(($lngRef - $previousPosition->lng) / $dLng * $dDate + $previousTimeStamp);
            } else if ($lngRef == $currentPosition->lng) {
                return DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
            }
            $index++;
        }
        return null;
    }

    // returns timestamp or null if not passed yet
    private function getEquatorUpDate($positions)
    {
        $latRef = 0;
        $index = 1;
        while ($index < sizeof($positions)) {
            $currentPosition = $positions[$index];
            if ($latRef > $currentPosition->lat) {
                $previousPosition = $positions[$index - 1];
                $previousTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $previousPosition->date)->getTimestamp();
                $currentTimeStamp = DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
                $dLat = abs($currentPosition->lat  - $previousPosition->lat);
                $dDate = $currentTimeStamp - $previousTimeStamp;
                return intval(($latRef - $previousPosition->lat) / $dLat * $dDate + $previousTimeStamp);
            } else if ($latRef == $currentPosition->lat) {
                return DateTime::createFromFormat(self::$DATE_FORMAT, $currentPosition->date)->getTimestamp();
            }
            $index++;
        }
        return null;
    }

    // returns timestamp or null if not passed yet
    private function getFinishDate($positions)
    {
        return null;
    }
}
